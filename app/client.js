/* system modules. */
const utility_node=require('util');
const zoneinfo=require('moment-timezone');
const fs=require('fs');

/* internal modules. */
const utility_t=require('../lib/utility');
const router_t=require('../lib/router');
const memcache_t=require('../lib/memcache');
const log_t=require('../lib/log');
const session_t=require('../lib/session');

/* client session. */
async function session_api(request, response) {
	log_t.info('/client/session');

	/* validation. */
	let object={
		device_id:''
	};

	/* router. */
	let router=await router_t.init(request, response, 'client', object);
	if (!router.input_valid)
		return router.fail(router_t.exception('client::session', configuration.error.AUTHORIZATION_INVALID, 'authorization failed.'));

	try {
		/* i/o object. */
		router.object('client');

		/* create new session. */
		let authentication={
			ip:router.request.ip,
			player_id:router.client.input.device_id,
			date:new Date().getTime()
		}
		let session=await session_t.create(authentication);
		router.session_update(session);

		/* fetch player. */
		let player=await memcache_t.fetch('player', router.session.object.player_id);

		/* set session. */
		router.client.output.app=configuration.app;
		router.client.output.player=player;
		return router.success();
	}
	catch (exception) {
		return router.fail(exception);
	}
}

/* api. */
exports.session_api=session_api;
