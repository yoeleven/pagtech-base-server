/* system modules. */
const utility_node=require('util');

/* internal modules. */
const utility_t=require('../lib/utility');
const router_t=require('../lib/router');
const log_t=require('../lib/log');

/* general ping. */
async function ping_api(request, response)
{
	log_t.info('/general::ping');

	let object=null;

	try {
		/* router. */
		let router=await router_t.init(request, response, 'ping', object);

		/* input/output. */
		return router.success();
	}
	catch (exception) {
		return router.fail(exception);
	}
}

/* api. */
exports.ping_api=ping_api;
