/* system modules. */
const utility_node=require('util');
const zoneinfo=require('moment-timezone');
const fs=require('fs');

/* internal modules. */
const utility_t=require('../../lib/utility');
const router_t=require('../../lib/router');
const memcache_t=require('../../lib/memcache');
const log_t=require('../../lib/log');
const session_t=require('../../lib/session');

/* [DEVELOPER NOTE] define game configuration. */
var config={
	/*
	 * [DEVELOPER NOTE] these configuration options are required and used
	 * in the game management system. fill in the fields for title and index.
	 */
	id:14, type:'tiago-bingo',
	title:'<Game Name>', index:'<Game Name All Lowercase All One Word>', version:"1.0",
	enabled:{mobile:false, browser:false, flash:false},
	display:{mobile:false, browser:false, flash:false},
	level_required:1, categories:[],

	/*
	 * [DEVELOPER NOTE] the rest of this object is only here as a reference.
	 * replace it with your own confuration.
	 */
	ball_count:75, draw_count:40, draw_extra_count:8,
	card_count:4, spot_count:20,
	row_count:5, column_count:4,
	pattern_count:13, payline_count:20,

	/* ball index. */
	bingo_list:[
		[1,15],[16,30],[31,45],[46,60],[61,75]
	],
	/* bingo patterns. */
	/*
		B  I  N  G  O
		00 01 02 03 04
		05 06 07 08 09
		10 11 12 13 14
		15 16 17 18 19
	*/
	pattern_list:[
		{name:'Four Corners',paytable:2,pattern:[1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1]}, /* 4 corners. */
		{name:'One Row',paytable:3,pattern:[1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]}, /* 1 row. */
		{name:'Crazy V',paytable:8,pattern:[1,0,0,0,1,0,1,0,1,0,0,0,1,0,0,0,0,0,0,0]}, /* crazy v. */
		{name:'Crazy T',paytable:8,pattern:[1,1,1,1,1,0,0,1,0,0,0,0,1,0,0,0,0,1,0,0]}, /* crazy t. */
		{name:'Two Columns',paytable:16,pattern:[0,1,0,1,0,0,1,0,1,0,0,1,0,1,0,0,1,0,1,0]}, /* 2 columns. */
		{name:'Two Rows',paytable:25,pattern:[1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0]}, /* 2 rows. */
		{name:'Wings',paytable:40,pattern:[1,0,0,0,1,0,1,0,1,0,0,1,0,1,0,1,0,0,0,1]}, /* wings. */
		{name:'Brackets',paytable:75,pattern:[1,1,0,1,1,1,0,0,0,1,1,0,0,0,1,1,1,0,1,1]}, /* brackets. */
		{name:'Crazy M',paytable:125,pattern:[1,0,0,0,1,1,1,0,1,1,1,0,1,0,1,1,0,0,0,1]}, /* crazy m. */
		{name:'Three Columns',paytable:220,pattern:[1,0,1,0,1,1,0,1,0,1,1,0,1,0,1,1,0,1,0,1]}, /* 3 columns. */
		{name:'Three Rows',paytable:400,pattern:[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0]}, /* 3 rows. */
		{name:'Picture Frames',paytable:750,pattern:[1,1,1,1,1,1,0,1,0,1,1,0,1,0,1,1,1,1,1,1]}, /* picture frames. */
		{name:'Coverage',paytable:1500,pattern:[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]} /* all spots. */
	],
	payline_list:[
		{id:'row',pattern_index_list:[1,5,10,-1],spot_mask:parseInt('11111000000000000000',2)}, /* 1st row (group). */
		{id:'row',pattern_index_list:[1,5,10,-1],spot_mask:parseInt('00000111110000000000',2)}, /* 2nd row (group). */
		{id:'row',pattern_index_list:[1,5,10,-1],spot_mask:parseInt('00000000001111100000',2)}, /* 3rd row (group). */
		{id:'row',pattern_index_list:[1,5,10,-1],spot_mask:parseInt('00000000000000011111',2)}, /* 4th row (group). */
		{id:'column',pattern_index_list:[-1,4,9,9,-1],spot_mask:parseInt('10000100001000010000',2)}, /* 1st column (group). */
		{id:'column',pattern_index_list:[-1,4,9,9,-1],spot_mask:parseInt('01000010000100001000',2)}, /* 2nd column (group). */
		{id:'column',pattern_index_list:[-1,4,9,9,-1],spot_mask:parseInt('00100001000010000100',2)}, /* 3rd column (group). */
		{id:'column',pattern_index_list:[-1,4,9,9,-1],spot_mask:parseInt('00010000100001000010',2)}, /* 4th column (group). */
		{id:'column',pattern_index_list:[-1,4,9,9,-1],spot_mask:parseInt('00001000010000100001',2)}, /* 5th column (group). */
		{id:'fourcorners',pattern_index_list:[0],spot_mask:parseInt('10001000000000010001',2)}, /* 4 corners. */
		{id:'vdefault',pattern_index_list:[2],spot_mask:parseInt('10001010100010000000',2)}, /* v default. */
		{id:'vinverse',pattern_index_list:[2],spot_mask:parseInt('00000001000101010001',2)}, /* v inverse. */
		{id:'tdefault',pattern_index_list:[3],spot_mask:parseInt('11111001000010000100',2)}, /* t default. */
		{id:'tinverse',pattern_index_list:[3],spot_mask:parseInt('00100001000010011111',2)}, /* t inverse. */
		{id:'wings',pattern_index_list:[6],spot_mask:parseInt('10001010100101010001',2)}, /* wings. */
		{id:'brackets',pattern_index_list:[7],spot_mask:parseInt('11011100011000111011',2)}, /* brackets. */
		{id:'mdefault',pattern_index_list:[8],spot_mask:parseInt('10001110111010110001',2)}, /* m default. */
		{id:'minverse',pattern_index_list:[8],spot_mask:parseInt('10001101011101110001',2)}, /* m inverse. */
		{id:'pictureframes',pattern_index_list:[11],spot_mask:parseInt('11111101011010111111',2)}, /* picture frames. */
		{id:'coverage',pattern_index_list:[12],spot_mask:parseInt('11111111111111111111',2)}, /* coverage. */
	]
};


/* [DEVELOPER NOTE] game implementation. */
async function game_function(session, input, player) {
	log_t.debug('game function.');
}

/* game play api entry point. */
async function play_api(request, response) {
	log_t.info('/tiago-bingo/play');

	/* [DEVELOPER NOTE] define the object input here. */
	let object={
		/*
		 * [DEVELOPER NOTE] all the information in this object is here as a
		 * reference. replace this data with your own input definition.
		 */
		bet:0, /* bet amount. */
		action:'', /* game | ball | card | play | play_external. */
		card_selection:0, /* card selection for getting a new card. */
		item_selection:0 /* card number selection for reveal. */
	};

	log_t.debug('authorize api call...');

	/* router. */
	let router=await router_t.init(request, response, 'bingo', object);
	if (!router.session_valid)
		return router.fail(router_t.exception('tiago-bingo::play', configuration.error.SESSION_INVALID, 'session expired.'));
	if (!router.input_valid)
		return router.fail(router_t.exception('tiago-bingo::play', configuration.error.AUTHORIZATION_INVALID, 'authorization failed.'));

	log_t.debug('authorization complete.');

	try {
		log_t.debug('run play.');

		/* register api object. */
		router.object('bingo');

		/* [DEVELOPER NOTE] load player. */
		let player=await memcache_t.fetch('player', router.session.object.player_id);

		/* [DEVELOPER NOTE] play game, update player object, get bingo result. */
		let game=await game_function(router.session.object, router.bingo.input, player);

		/* [DEVELOPER NOTE] save player object. */
		let result=await memcache_t.update('player', router.session.object.player_id, player);

		/* set session. */
		router.bingo.output.game=game;
		router.bingo.output.player=player;
		return router.success();
	}
	catch (exception) {
		return router.fail(exception);
	}
}

exports.play_api=play_api;
