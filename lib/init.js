/* system modeules. */
let memcached_t=require('memcached');
let clone=require('clone');
let utility_node=require('util');

/* internal modeules. */
const utility_t=require('../lib/utility');
const router_t=require('./router');
const log_t=require('../lib/log');

function memcache_error(detail) {
	log_t.error('[init::memcache_error] '+utility_node.inspect(detail));
}

/* init. */

async function load(option)
{
	let system={};

	/* memcache. */
	system.memcache=new memcached_t(option.memcache.server_list, option.memcache.configuration);
	system.memcache.on('issue', memcache_error);
	system.memcache.on('failure', memcache_error);
	system.memcache.on('reconnecting', memcache_error);
	system.memcache.on('reconnect', memcache_error);
	system.memcache.on('remove', memcache_error);

	/* configuration. */
	system.configuration={
		apikey:'KandbfJAbB3BdbfBjf323B42JB4jb2J4',
		default:{
			expire:86400
		},
		error:{
			UNKNOWN:1000,
			APIKEY_INVALID:1001,
			LOGIN_INVALID:1002,
			SESSION_INVALID:1003,
			INPUT_INVALID:1004,
			AUTHORIZATION_INVALID:1005
		},
		session:{
			expire:86400
		},
		player:{
			definition:{
				credit:1000,
				gem:0,
				level:1
			},
			expire:86400
		},
		app:{
		}
	};

	return system;
}

/* internal. */
exports.load=load;
