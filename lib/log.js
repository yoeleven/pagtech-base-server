/* system modules. */
const utility_node=require('util');

const LEVEL=[
	'debug',
	'info',
	'warn',
	'error'
];

function message(level, data)
{
	if (!option.logging)
		return;

	for (let index=0; index<option.logging.length; index++) {
		if (option.logging[index]==level) {
			console.log('['+level+'] '+data);
			return;
		}
	}
}

function debug(data) {
	if (!option.logging)
		return;

	message(LEVEL[0], data);
}

function info(data) {
	if (!option.logging)
		return;

	message(LEVEL[1], data);
}

function warn(data) {
	if (!option.logging)
		return;

	message(LEVEL[2], data);
}

function error(data) {
	if (!option.logging)
		return;

	message(LEVEL[3], data);
}

function audit(router) {
	if(!router) return;
	if(!router.request) return;
	if(!router.session) return;
	if(!router.session.object) return;

	definition.log.create({
		id_user:router.session.object.id_user,
		url:router.request.url
	});
}

/* internal. */
exports.debug=debug;
exports.info=info;
exports.warn=warn;
exports.error=error;
exports.audit=audit;
