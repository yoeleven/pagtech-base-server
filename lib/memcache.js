/* system modules. */
const utility_node=require('util');

/* internal modules. */
const utility_t=require('../lib/utility');
const router_t=require('./router');
const log_t=require('../lib/log');

const EXPIRATION_DEFAULT=3600;

function key_fetch(category, index) {
	if (!category)
		category='';
	if (!index)
		index='';

	return category+'-'+index;
}

function expiration_fetch(category) {
	if (!configuration || !category || !configuration[category] || configuration[category].expire)
		return EXPIRATION_DEFAULT;
	return configuration[category].expire;
}

async function touch(category, index) {
	return new Promise((resolve, reject)=>{
		let key=key_fetch(category, index);
		let expiration=expiration_fetch(category);
		log_t.debug('[memcache::touch] key: '+utility_node.inspect(key));
		return memcache.touch(key, expiration, (error, success)=>{
			if (error)
				throw router_t.exception('memcached', configuration.error.UNKNOWN, error);
			return resolve(success);
		});
	});
}

async function fetch(category, index) {
	return new Promise((resolve, reject)=>{
		let key=key_fetch(category, index);
		log_t.debug('[memcache::fetch] key: '+utility_node.inspect(key));
		return memcache.get(key, (error, object)=>{
			if (error)
				throw router_t.exception('memcached', configuration.error.UNKNOWN, error);
			return resolve(object);
		});
	});
}

async function update(category, index, object) {
	return new Promise((resolve, reject)=>{
		let key=key_fetch(category, index);
		let expiration=expiration_fetch(category);
		log_t.debug('[memcache::update] key: '+utility_node.inspect(key));
		return memcache.set(key, object, expiration, (error, success)=>{
			if (error)
				throw router_t.exception('memcached', configuration.error.UNKNOWN, error);
			return resolve(success);
		});
	});
}

async function remove(category, index) {
	return new Promise((resolve, reject)=>{
		let key=key_fetch(category, index);
		log_t.debug('[memcache::remove] key: '+utility_node.inspect(key));
		return memcache.del(key, (error)=>{
			if (error)
				throw router_t.exception('memcached', configuration.error.UNKNOWN, error);
			return resolve(true);
		});
	});
}

/* internal. */
exports.touch=touch;
exports.fetch=fetch;
exports.update=update;
exports.remove=remove;
