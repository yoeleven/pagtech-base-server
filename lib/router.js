/* system modules. */
const https=require('https');
const utility_node=require('util');

/* internal modules. */
const utility_t=require('../lib/utility');
const router_t=require('./router');
const log_t=require('../lib/log');
const validate_t=require('./validate');
const session_t=require('./session');

const APIKEY_LENGTH=32;

const OBJECT_RESERVED=[
	'request',
	'response',
	'apikey',
	'input',
	'output',
	'session',
	'object',
	'object_list',	
	'error',	
	'valid',	
	'fail',
	'success',
	'authorize'
];

function exception(category, code, message) {
	if (!category)
		category='';
	if (!code)
		code='';
	if (!message)
		message='';
	return {category:category,code:code,message:message};
}

/* create stack context and return. */
async function init(request, response, input_key, input_definition) {
	let router={
		/* http context. */
		request:request,
		response:response,
		apikey:request.headers['api-key'],
		input:request.body,
		output:{},

		/* state data. */
		session:{
			input:null,
			output:null,
			object:null
		},
		object_list:[],

		/* validation flags. */
		input_valid:false,
		apikey_valid:false,
		session_valid:false,

		/* set the session object. */
		session_update:function (object) {
			if (!this.session) {
				log_t.error('[router::session_update] session is null.');
				return;
			}
			this.session.input=this.input.session;
			this.session.object=object;
			this.session.output={hash:object.id}
			this.session_valid=true;
		},

		/* validation session input. */
		authorize:async function() {
			/* validate apikey. */
			if (!this.apikey || this.apikey.trim()=='' || this.apikey.trim().length!=APIKEY_LENGTH) {
				log_t.error('[router::authorize] invalid api key.');
				return false;
			}

			/* validate apikey. */
			if (this.apikey!=configuration.apikey) {
				log_t.error('[router::authorize] incorrect api key.');
				return false;
			}

			/* validate session object. */
			if (!validate_t.object(session_t.object(), this.input.session)) {
				log_t.error('[router::authorize] invalid session object.');
				return false;
			}

			/* validate input definition. */
			if (!input_key || !input_definition) {
				log_t.error('[router::authorize] invalid input definition.');
				return false;
			}

			/* validate input object. */
			if (!this.input || !this.input[input_key]) {
				log_t.error('[router::authorize] invalid input object.');
				return false;
			}

			/* validate input with definition. */
			if (!validate_t.object(input_definition, this.input[input_key])) {
				log_t.error('[router::authorize] invalid input.');
				return false;
			}

			this.input_valid=true;

			try {
				let session=await session_t.fetch(this.input.session.hash);
				if (!session) {
					log_t.warn('[router::authorize] session invalid.');
					return false;
				}
				let result=await session_t.touch(session.id);
				this.session_update(session);
			} catch (exception) {
				throw exception;
			}

			return true;
		},

		/* validate general input. */
		object:function(object_name) {
			if (!object_name || object_name.trim()=='') {
				log_t.error('[router::object] object name is null.');
				return;
			}
			if (utility_t.array_value_find_index(OBJECT_RESERVED, object_name)!=-1) {
				log_t.error('[router::object] the object \''+object_name+'\' is a reserved object.');
				return;
			};
			if (this[object_name]) {
				log_t.warn('[router::object] the object name \''+object_name+'\' is a reserved object.');
				return;
			}

			this[object_name]={};
			this[object_name].input={};
			this[object_name].output={};
			if (this.input[object_name])
				this[object_name].input=this.input[object_name];
			this.object_list.push(object_name);
			this.input[object_name]=null;
		},

		/* error response handler.. */
		fail:function(exception) {
			if (!exception)
				exception={};
			log_t.error('['+exception.category+'] '+exception.message+' ('+exception.code+')');
			if (!this.response.headersSent)
				this.response.send({status:0, error:exception.code});
		},

		/* success response handler.. */
		success:function() {
			if (!this.session_valid)
				return this.fail(exception('invalid session', configuration.error.SESSION_INVALID, 'Session Error'));

			try {
				this.output.status=1;
				this.output.session=router.session.output;
				for (let index=0; index<this.object_list.length; index++) {
					let object_name=this.object_list[index];
					if (Object.keys(this[object_name].output).length==0)
						continue;
					this.output[object_name]=this[object_name].output;
				}
				if (!this.response.headersSent)
					this.response.send(this.output);
			}
			catch (error) {
				log_t.error('[router::success] '+error);
			}
		}
	};

	/* validate session. */
	try {
		let valid=await router.authorize();
	}
	catch (exception) {
		log_t.warn('['+exception.category+'] '+exception.message+' ('+exception.code+')');
	}

	return router;
}

/* internal. */
exports.APIKEY_LENGTH=APIKEY_LENGTH;
exports.exception=exception;
exports.init=init;
