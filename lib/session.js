/* system modules. */
const utility_node=require('util');
const clone=require('clone');

/* internal modules. */
const utility_t=require('../lib/utility');
const router_t=require('./router');
const memcache_t=require('./memcache');
const log_t=require('./log');

/* session object. */
function object() {
	return {
		hash:''
	};
}

async function create(authentication)
{
	/* create session hash. */
	let key=authentication.login+authentication.ip+authentication.date;
	let hash=null;
	try {
		hash=utility_t.sha256_hash(key, 'hex');
		console.log('hash: '+hash);
	} catch(error) {
		throw router_t.exception('md5', configuration.error.UNKNOWN, error);
	};

	/* load player. */
	let player=await memcache_t.fetch('player', authentication.player_id);
	log_t.debug('player: '+utility_node.inspect(player));
	/* new player. */
	if (!player) {
		player=clone(configuration.player.definition);
		let player_result=await memcache_t.update('player', authentication.player_id, player);
	}

	/* login session. */
	let session={
		id:hash,
		ip:authentication.ip,
		player_id:authentication.player_id,
		date:authentication.date
	};
	/* add session. */
	let session_result=await memcache_t.update('session', session.id, session);

	return session;
}

async function touch(id)
{
	let result=await memcache_t.touch('session', id);
	return result;
}

async function fetch(id)
{
  let session=await memcache_t.fetch('session', id);
	return session;
}

/* internal. */
exports.object=object;
exports.create=create;
exports.touch=touch;
exports.fetch=fetch;
