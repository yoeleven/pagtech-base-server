/* system modules. */
const utility_node=require('util');
const moment=require('moment-timezone');
const crypto=require('crypto');
const clone=require('clone');
const mersenne=require('mersenne');

/* internal modules. **/
const utility_t=require('../lib/utility');
const router_t=require('./router');
const log_t=require('../lib/log');

/* init mersenne */
//var bit=4294967296;
var bit=new Date().getTime();
var seed=Math.floor(Math.random()*bit);
mersenne.seed(seed);

/* array/hash search, sort, replace, etc. ----------------------------------- */

function array_value_find_index(array_in, value_in) {
	if (!array_in)
		return -1;
	if (!value_in)
		return -1;
  for (let index=0; index<array_in.length; index++) {
    if (array_in[index]==value_in) {
      return index;
    }
  }
	return -1;
}

function array_index_remove(array_in, index_in) {
  let array_copy=[];

  /* copy element pointers to another array. */
  for (let element of array_in) {
    array_copy.push(element);
  }

  /* pop all the element off this array. */
  for (let element of array_copy) {
    array_in.pop();
  }

  /* add elements back in, excluding the matching element. */
  for (let index in array_copy) {
    if (parseInt(index)!=index_in) {
      array_in.push(array_copy[index]);
    }
  }

  /* return the original array. */
  return array_in;
}

function array_key_value_find(array_in, key_in, value_in) {
	if (!array_in || !array_in.length>0)
		return null;

  /* object key/value match. */
  for (let element of array_in) {
    if (element[key_in]==value_in) {
      /* return the matching array element. */
      return element;
    }
  }
  return null;
}

function array_key_value_remove(array_in, key_in, value_in) {
  let array_copy=[];

  /* copy element pointers to another array. */
  for (let element of array_in) {
    array_copy.push(element);
  }

  /* pop all the element off this array. */
  for (let element of array_copy) {
    array_in.pop();
  }

  /* add elements back in, excluding the matching element. */
  for (let element of array_copy) {
    if (element[key_in]!=value_in) {
      array_in.push(element);
    }
  }

  /* return the original array. */
  return array_in;
}

/* type helpers. ------------------------------------------------------------ */

function string_set(value) {
	if (value==null || value==undefined || value.trim()=='')
		return undefined;
	return value;
}

function number_set(value) {
	if (value==null || value==undefined || Number.parseFloat(value)==NaN || value==NaN)
		return undefined;
	return Number.parseFloat(value);
}

function array_set(value) {
	if (value==null || value==undefined)
		return null;
	if (typeof(value)!=='object') {
		if (typeof(value)==='string' && value.trim()=='')
			return null;
		return [value];
	}

	return value;
}

/* encryption. --------------------------------------------------------------- */

function string_encrypt(text, algorithm, key) {
	let cipher=crypto.createCipher(algorithm, key);
	let crypted=cipher.update(text, 'utf-8', 'hex');
	crypted+=cipher.final('hex');
	return crypted;
}

function string_decrypt(text, algorithm, key) {
	let decipher=crypto.createDecipher(algorithm, key)
	let decrypted=decipher.update(text, 'hex', 'utf8')
	decrypted+=decipher.final('utf8');
	return decrypted;
}

function sha256_hash(data, type) {
	const sha256=crypto.createHash('sha256');
	sha256.update(data);
	const hash=sha256.digest(type);
	return hash;
}

/* randomize. ---------------------------------------------------------------- */

function string_random(length) {
  let characterset='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  let result='';
  for (let index=length; index>0; index--)
    result+=characterset[Math.floor(Math.random()*characterset.length)];
  return result;
}

function number_random(min, max) {
  return Math.floor(Math.random()*(max-min+1)+min);
}

/* internal. */
exports.array_value_find_index=array_value_find_index;
exports.array_index_remove=array_index_remove;
exports.array_key_value_find=array_key_value_find;
exports.array_key_value_remove=array_key_value_remove;
exports.string_set=string_set;
exports.number_set=number_set;
exports.array_set=array_set;
exports.string_encrypt=string_encrypt;
exports.string_decrypt=string_decrypt;
exports.sha256_hash=sha256_hash;
exports.string_random=string_random;
exports.number_random=number_random;
