/* system modules */
const utility_node=require('util');

/* internal modules. */
const log_t=require('./log');

function traverse(data_validate, data_input) {

	/* check for null. */
	if (data_validate==null || data_input==null) {
		log_t.warn("[validate::traverse] null ("+data_validate==null+") || null ("+data_input==null+")");
		return false;
	}

	/* check type match. */
	if (typeof(data_validate)!==typeof(data_input)) {
		log_t.warn("[validate::traverse] type "+data_validate+" ("+typeof(data_validate)+") != "+data_input+" ("+typeof(data_input)+")");
		return false;
	}

	/* object. */
	if (typeof(data_validate)==='object') {
		for (let key in data_validate) {
			if (!data_input.hasOwnProperty(key)) {
				log_t.warn("[validate::traverse] object !"+key+" ("+data_validate[key]+")");
				return false;
			}
			if (!traverse(data_validate[key], data_input[key])) {
				log_t.warn("[validate::traverse] object !"+key+" ("+data_validate[key]+")");
				return false;
			}
		}
	}

	return true;
}

function object(object_validate, object_input)
{
	return traverse(object_validate, object_input);
}

/* internal. */
module.exports.object=object;
