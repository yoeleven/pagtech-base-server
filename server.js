/* system modules. */
const fs=require('fs');
const path=require('path');
const optimist=require('optimist');
const express=require('express');
const express_parser=require('body-parser');
const express_multer=require('multer'); 
const http=require('http');
const https=require('https');
const utility_node=require('util');

/* lib modules. */
const utility_t=require('./lib/utility');
const init_t=require('./lib/init');
const router_t=require('./lib/router');
const log_t=require('./lib/log');
const memcache_t=require('./lib/memcache');

/* app modules. */
const general_t=require('./app/general');
const client_t=require('./app/client');

/* game modules. */
const tiago_bingo_t=require('./engine/tiago-bingo/main');

/* confiuration file. */
let option_id=(optimist.argv['config']!==undefined)?optimist.argv['config']:'none';
let debug=(optimist.argv['debug']!==undefined)?Boolean(parseInt(optimist.argv['debug'])):false;

/* option_id. */
let option_string=null;
try {
  option_string=fs.readFileSync(option_id+'.json');
} catch (error) {
  console.log(' configuration file: '+option_string);
  console.log(error);
  return;
}

/* option. */
option={};
try {
  option=JSON.parse(option_string);
} catch (error) {
  console.log('parsing JSON option: '+error);
  return;
}

log_t.info('[seerver::option] '+utility_node.inspect(option));

/* server/framework/services. */

let server=express();

/* ssl. */
let ssl_config=null;
if (option.server.protocol=='https') {
	ssl_config={
		key:fs.readFileSync(option.certificate.key),
		cert:fs.readFileSync(option.certificate.main),
		ciphers:'ECDHE-RSA-AES256-SHA:AES256-SHA:RC4-SHA:RC4:HIGH:!MD5:!aNULL:!EDH:!AESGCM'
	};
}

/* server headers. */
server.use(function (request, response, complete) {
  response.set('Access-Control-Allow-Origin', '*');
  response.set('Access-Control-Max-Age', '1000');
  response.set('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Origin, Authorization, Accept, Accept-Encoding, Api-Key');
  response.set('Access-Control-Allow-Methods', 'POST, GET, OPTIONS');
  return complete();
});

/* data parsing. */
server.use(express_parser.json({limit:'10mb'})); /* application/json */
server.use(express_parser.urlencoded({limit:'1mb',extended:true})); /* application/x-www-form-urlencoded */

/* globalize services. */
configuration=null;
memcache=null;

async function init()
{
	log_t.info('server::init');
	let system=await init_t.load(option);

	/* assign system objects to global references. */
	configuration=system.configuration;
	memcache=system.memcache;

	/* start node.js server. */
	try {
		if (option.server.protocol=='https') {
			https.createServer(ssl_config, server).listen(option.server.port);
		}
		else {
			http.createServer(server).listen(option.server.port);
		}
		log_t.info('[server::init] https: '+option.server.port);
	}
	catch (error) {
		log_t.error('[server::init] '+error);
	}
}

/* api endpoints. */

try {
	/* client. */
	server.post('/client/session', client_t.session_api);

	/* general. */
	server.get('/ping', general_t.ping_api);

	/* [DEVELOPER NOTE] bing play entry point.. */
	server.post('/tiago-bingo/play', tiago_bingo_t.play_api);

} catch(error) {
	log_t.error('[server] route does not exist: '+error);
}

init();
